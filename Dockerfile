FROM php:8.3

RUN apt update && apt -yqq install \
    libpq-dev libzip-dev libpng-dev libjpeg62-turbo-dev \
    libicu-dev procps unzip jpegoptim optipng pngquant \
    gifsicle nodejs npm

RUN docker-php-ext-configure gd --with-jpeg=/usr/include \
    && docker-php-ext-install sockets pdo pdo_pgsql opcache gd zip exif pcntl intl

RUN curl -sS https://getcomposer.org/installer | php && cp composer.phar /usr/bin/composer

CMD ["php"]
